# Tools

## Introduction

FG Tools provide a set of tools and Utils scripts used within FG but that can also be usefull for your app code.

## Integration Steps

1) **"Install"** or **"Upload"** FG Tools plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

## Debug

> <img src="_source/debugConsole.png" width="256" height="540" style="display: block; margin: 0 auto"/>

FunGames SDK provide some tools to debug main features and check what is happening within your game while you are running your app on device.

For example, adding the **FGDebugConsole prefab** (_Assets > FunGames > Tools > Debug_ folder) in your scene will provide you an overlay console that will enable you to test the integration of the main features of FG SDK. Since some SDKs used in FunGames doesn't work in the Unity Editor, this will help you checking that everything is ok, directly from your device.

You could also create your own custom console, by changing the size of the console window, or adding new menus through the FG Debug Console Builder component for example.

On FunGames SDK, Logging is enabled by default, but you can also customize it in the FGDebugSettings (_Resources > FunGames > Debug > FGDebugSettings_) in the Resources/Settings folder.

## Loading Screen

> <img src="_source/loadingScreen.png" width="256" height="540" style="display: block; margin: 0 auto"/>

Some modules can sometimes delay or create some laggy behaviors during their initialization. If you want, you can add a Loading Screen to your app through the **FGLoadingScreen component**. This component is intended to work with FunGames SDK, and will close automatically when all FG Modules are initialized, or after a maximum given delay.

You can also configure it to behave as a simple "Closing Panel" in your main Scene, or , if you want to make sure all FunGames modules are initialized before starting your game, you can add it (with all FG prefabs) in a Splash scene, chosing the "Load Next Scene" mode (this way, it will automatically load next scene after FG SDK is fully initialized). 

You can find some LoadingScreen example prefabs in **_Assets > FunGames > Tools > LoadingScreen_** folder.